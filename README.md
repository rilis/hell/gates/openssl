## OpenSSL proxy repository for hell

This is proxy repository for [openssl library](https://github.com/openssl/openssl), which allow you to build and install it using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of OpenSSL using hell, have build improvement idea, or want to request support for other existing versions of OpenSSL, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in OpenSSL or need any kind of information about OpenSSL usage itself please contact with OpenSSL authors. You can do this for example here : [openssl issue tracker](https://github.com/openssl/openssl/issues), because I'm not  OpenSSL developer and I'm not responsible or even capable to provide any kind of details about it.

## Important notes

This package require additional tools to be installed preperly - internaly original openssl build system is used, so you need to have installed on build machine at least:
  * perl
  * masm - if you are building on windows
  * make - if you are building on linux or mac os x (even if you will use other build tool in hell using "-G" option)
  * namke - if you are building on windows (even if you will use other build tool in hell using "-G" option)

Currently only supported CPU architectures are: 
  * x86 
  * AMD64/x86_64

Corrently only supported OS are:
  * Linux
  * Mac OS X
  * Windows